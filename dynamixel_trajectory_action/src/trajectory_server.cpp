#include "dynamixel_trajectory_action/trajectory_server.hpp"
#include <numeric>
#include <vector>

DynamixelTrajectoryAction::DynamixelTrajectoryAction(const std::string& name)
    :as_(nh_, name, boost::bind(&DynamixelTrajectoryAction::executeCB, this, _1), false),
    action_name_(name) {
  jointPub = nh_.advertise<sensor_msgs::JointState>("goal_dynamixel_position", 1);
  jointSub = nh_.subscribe<sensor_msgs::JointState>("joint_states", 1, boost::bind(&DynamixelTrajectoryAction::onJointState, this, _1));
  as_.start();
}

void DynamixelTrajectoryAction::onJointState(const sensor_msgs::JointState::ConstPtr& joints) {
  currentState = *joints;
}

void DynamixelTrajectoryAction::executeCB(const control_msgs::FollowJointTrajectoryGoalConstPtr& goal) {
  ROS_INFO_STREAM("onGoal(): Starting with " << goal->trajectory.points.size() << " points.");

  bool success = true;
  targetState = currentState;
  targetState.header.seq = 0;
  targetState.name = currentState.name;
  feedback_.joint_names = currentState.name;
  ros::Time startTime = ros::Time::now();

  for (size_t pointIdx = 0; pointIdx < goal->trajectory.points.size(); pointIdx++) {
    const trajectory_msgs::JointTrajectoryPoint& curPoint = goal->trajectory.points[pointIdx];
    ROS_INFO_STREAM("Point_" << pointIdx << "...");


    targetState.header.seq++;
    targetState.position = curPoint.positions;
    targetState.velocity = curPoint.velocities;
    targetState.effort = curPoint.effort;
    
    targetState.position.push_back(currentState.position[6]);
    targetState.velocity.push_back(currentState.velocity[6]);
    targetState.effort.push_back(0);

    if (as_.isPreemptRequested()) {
      ROS_INFO("Preempted at point %zd", pointIdx);
      as_.setPreempted();
      success = false;
      break;
    }

    targetState.header.stamp = ros::Time::now();
    jointPub.publish(targetState);

    ros::Duration sinceStart = ros::Time::now() - startTime;
    //ROS_INFO_STREAM("sinceStart: " << sinceStart);
    ros::Duration sleepDuration = curPoint.time_from_start - sinceStart;
    if (sleepDuration.toSec() > 0.0) {
      ROS_INFO_STREAM("sinceStart=" << sinceStart << " will sleep for " << sleepDuration);
      sleepDuration.sleep();
    }

    feedback_.desired = curPoint;
    feedback_.actual.positions = targetState.position;
    feedback_.actual.velocities = targetState.velocity;
    feedback_.actual.effort = targetState.effort;

    sinceStart = ros::Time::now() - startTime;
    feedback_.actual.time_from_start = sinceStart;

    feedback_.error.positions = targetState.position - feedback_.actual.positions;
    feedback_.error.velocities = targetState.velocity - feedback_.actual.velocities;
    feedback_.error.effort = targetState.effort - feedback_.actual.effort;

    as_.publishFeedback(feedback_);

    bool limit_exceeded = false;
    for (size_t jointIdx = 0; jointIdx < feedback_.error.positions.size(); jointIdx++) {
      if (feedback_.error.positions[jointIdx] > max_position_error) {
        limit_exceeded = true;
        break;
      }
    }

    if (limit_exceeded) {
      ROS_ERROR_STREAM("Too high position error");
      result_.error_code = result_.PATH_TOLERANCE_VIOLATED;
      as_.setAborted(result_);
      success = false;
      break;
    }
  }

  if (success) {
    result_.error_code = result_.SUCCESSFUL;
    ROS_INFO_STREAM("Current trajectory complete");
    as_.setSucceeded(result_);
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "trajectory_action_server");

  ROS_INFO("Starting action server!");
  DynamixelTrajectoryAction action("arm/joint_trajectory_action");

  ROS_INFO("Spinning...");
  ros::spin();

  return 0;
}
